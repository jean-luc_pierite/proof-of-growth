# Proof of Growth

Proof of growth is a system that rewards people who maintain optimal growth conditions for plants in urban vertical farms. Tokens are generated and rewarded based upon nutrient levels, temperature, and humidity. This will connect to a global exchange promoting cities with the most productive urban farms. A web platform maps and renders farms by height and "green" impacts.

How to add lumens?
https://electronics.stackexchange.com/questions/172866/how-to-add-lumens

What is a lux meter?
https://www.hackster.io/embeddedlab786/diy-light-lux-meter-ad3a81

What is a Passive Infrared Sensor?
https://www.seeedstudio.com/blog/2019/08/03/pir-sensor-introduction-and-how-pir-motion-sensor-works-with-arduino-and-raspberry-pi/

Sawdust Clay for modling and casting
http://www.stephsuniverse.com/crafts/sawdust_flour_clay.htm
